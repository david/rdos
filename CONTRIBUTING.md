# CONTRIBUTING

## Merge Request Process

- Pour chaque tâche à effectuer, créer une nouvelle branche dont le nom indique
l'objectif de la nouvelle tâche.

- Respecter les règles pour obtenir un code propre

- chaque merge request doit être accompagnée de tests ajoutées dans le répertoire test.

- Le nom du développeur et celui du relecteur de la merge request apparaissent dans l'entête
de chaque ficher de code.

## Tests unitaires

- Le nom de chaque fichier doit commencer par `test_`
- Le nom de chaque fonction doit commencer par `test_`

## Code Propre

- Les commentaires précédents une fonction ne doivent pas être séparés de celle ci par des sauts de ligne
- Une fonction doit avoir un nom explicite.
- Le nom des variables globales suit la syntaxe suivante __RDOS_NOM__
- Chaque fonction doit être commentée. Les commentaires de chaque fonction contiennent une explication sur les paramètres
de la fonction et sur les différentes valeurs de retour.
- pour du code Python, on essaiera d'expliciter au maximum le type des paramètres de chaque fonction.

### Exemple

`def ma_fonction(x, chaine)` n'est pas assez explicite.

`def ma_fonction(x:int, chaine: str)` est préféré.



