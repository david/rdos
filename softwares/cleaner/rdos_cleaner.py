import os
from os import listdir
import time


# Routine that deletes old directories
def clean_old_directories(path: str, max_age:int):
    directories_path = os.path.join(path,"jobs")
    for f in listdir(directories_path):
        if f != "index.html":
            job_path = os.path.join(directories_path,f)
            st = os.stat(job_path)
            age = time.time() - st.st_mtime
            if (age > max_age): #by default, after two days, directory is deleted
                for file in listdir(job_path):
                    os.remove(os.path.join(job_path, file))
                    os.rmdir(job_path)
                    print("Dossier ", job_path, "nettoyé")


working_dir = "/home/david/rdos/"
if os.getenv("working_dir") != None:
    working_dir = os.getenv("working_dir")

max_age = 172800 # two days in seconds
if os.getenv("max_age") != None :
    working_dir = int(os.getenv("max_age"))


while 1:
    clean_old_directories(working_dir, max_age)
    time.sleep(1)
