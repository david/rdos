#!/usr/bin/python
import smtplib
import socket




def success(email: str, jobid:str):
       with open("../mailer/templates/rdos_success.txt") as file:
              message = file.read().replace("{{email}}",email).replace("{{jobid}}", jobid)
       return message


def failed(email: str, error_message:str):
       with open("../mailer/templates/rdos_failure.txt") as file:
              message = file.read().replace("{{email}}",email).replace("{{error_message}}", error_message)
       return message

def send_mail(email: str, job_id:str) :
       if len(email) > 0:
              try:
                     smtpObj = smtplib.SMTP('mail.lipn.univ-paris13.fr')
                     smtpObj.sendmail('rdos@lipn.univ-paris13.fr',
                                      email, success(email, job_id))
                     print("Successfully sent email to: ", email)
                     return 1
              except smtplib.SMTPException:
                     print("Error: unable to send email")
                     return -1
              except socket.timeout:
                     print("Time Out, could not connect to server")
                     return -1
       else:
              print("Email not sent (Empty email address)")
              return 0



