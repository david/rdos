#!/usr/bin/env python3
# authors: Julien DAVID & Ismail MOUMNI
import socket
import json
import datetime
import os
import sys
sys.path.append(os.path.realpath('../softwares/'))
import database as db  # noqa E402

__RDOS_Dict__ = {"parameters": "request"}

# tools List
__RDOS_Tool__ = {}


class RdosServer:

    # Function server_conn creates a socket and listens on port 9393
    # Function input address : SERVER IP ADRESS & Port server Port
    # Function output query send from client to server
    def server_conn(address, PORT):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as serv:
            serv.bind((address, PORT))
            # Listening to 5 CLients
            serv.listen(5)
            while True:
                # accepting the connection from client and getting client IP
                conn, addr = serv.accept()
                if conn:
                    print('Connexion acceptee depuis l IP : ', addr)
                    # Receiving Data from Client
                    data = conn.recv(4096)
                    query = (json.loads(data.decode('utf-8')))
                    print("Requete reçu : ", query)
                    if (query is not None):
                        print(query)
                        query_process(conn, query)
                        conn.close()


# Function query_process checks query to get generators or to insert query
# Function takes 2 parameters socket and query (get generator query or to insert in database a new job)
# Function returns generators list to client or returns insert result to client
def query_process(sock: socket, query: dict):
    # Sending GENERATORS
    if (match_query_dict(query, __RDOS_Dict__)):
        response = db.get_generators()
        sock.send(bytes(json.dumps(response), "utf-8"))
        print("database generators ", response)

    # Sending default generator values
    elif ((list(query.keys()))[0] == "default"):
        gen = query["default"]
        print("generator :", gen)
        resp = get_generator_parameters_for_client({gen})
        sock.send(bytes(json.dumps(resp), "utf-8"))

    # Inserting client query in DATABASE
    else:
        print("check", check_and_complete_parameters(query))
        res = db_send_job(sock, query_fields(sock, check_and_complete_parameters(query)))
        print(res)


# Function query_fields generates default fields to insert query send from client to database
# Function takes 2 parameters addr : IP address of client and data : Generators values
# Function return a valid insert query
def query_fields(addr, data):
    __RDOS_Tool__ = db.get_generator_list()
    sa = __RDOS_Tool__.get(list(data.keys())[0])
    lm = list((data.values()))
    s = {"id": "", "idGenerator": "", "IP": "", "timeSubmitted": "", "timeExecuted": "",
         "timeFinished": "", "status": "", "parametersJSON": "", "directory": "", "url": "", "message": "", "email": ""}
    s["id"] = str(os.getuid())
    s["idGenerator"] = str(sa)
    s["IP"] = addr.getpeername()[0]
    s["timeSubmitted"] = (str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
    s["parametersJSON"] = json.dumps(lm[0])
    return s


# Function Client Generator gets from database a generator paramaters and default parameters
# Function takes a dict generator to send to database
# Function outputs the result from database
def get_generator_parameters_for_client(generator: dict):
    if generator is not None:
        res = db.getParameters(generator)
        return res
    else:
        return Exception("Generator Vide !!")


# Function db_send_job send a query to insert in database by socket
# Function takes a socket and dictionary for input
# Function returns a string if query send
def db_send_job(s: socket, req: dict):
    if(req is not None):
        ins = db.add_job(req)
        s.send(bytes(json.dumps(ins), "utf-8"))
        print("Query send")
    else:
        raise Exception("Empty Query")


# Function query_valid matches the query send from client side with the the parameter dict
# Function input dictionary from client
# Function output boolean true if match else False
def query_valid(data: dict):
    if 'parameters' in data:
        if (data['parameters'] != '') & (len(data) <= 2):
            return True
        else:
            return False


# Function json_to_dict changes a json file into a Dict
# Function json_to_dict returns a dictionary
def json_to_dict(data):
    if data is not None:
        result = json.loads(data)
        return result
    else:
        raise Exception("Dictionnaire Vide!!")


# Function db_req sends a request to get parameters of tools from database
# Function takes a dictionary for input
# Function return a Dict containing tool parameters and default parameters
def db_req(tool: dict):
    if dict is not None:
        tool_dict = db.getParameters(tool)
        return tool_dict
    else:
        raise Exception("Dictionnaire Vide!!")


# Function missing_keys returns non existing fields in a dict
# Function input biblio and dict
# Function returns a list containing missing values
def missing_keys(biblio: dict, gen: dict):
    missing = []
    if(biblio is not None and gen is not None):
        for key in biblio:
            if key not in gen:
                missing.append(key)
        return missing
    else:
        raise Exception("Dictionnaire Vide!!")


# Function match_query_dict matches two dictionaries
# Function input takes 2 dictionaries to match
# Function output return trur if dictionaries matches
def match_query_dict(biblio: dict, data: dict):
    if biblio is not None and data is not None:
        return biblio.keys() == data.keys()
    else:
        raise ValueError("Dictionnaire Vide ")


# Function check_and_complete_parameters verifies json send from client to server by matching it with database query
# Function input takes a dictionary
# Function output returns query if it matches with database query keys
def check_and_complete_parameters(data: dict):
    if data is not None:
        tool = list(data.keys())[0]
        s = db.getParameters({tool})
        parameter = {}
        if s is not None:
            # gen = s[0]
            parameter = s[1]
            q = data.get(tool)
            if match_query_dict(parameter.get(tool), q):
                check_and_replace(data.get(tool), parameter.get(tool))
                return data
            else:
                raise Exception("Query Doesn't match")
        else:
            raise Exception("Error Database")
    else:
        return Exception("Dictionnaire Vide!!", tool)


# check_and_replace checks query with default_parameters of generator and replaces empty values with default_parameters
# Function input takes 2 dictionaries
# Function output returns matched query
def check_and_replace(query: dict, default_parameters: dict):
    if query is not None and default_parameters is not None:
        for a, b in query.items():
            if b == '':
                query[a] = default_parameters.get(a)
        return query
    else:
        raise Exception("Dictionnaire Vide!!")


# Function db_generators returns all database generators
# Function takes a dictionary request from client side
# Function returns all database generators or raises an error if request doesn't match
def db_generators(req: dict):
    if req is not None:
        match = {"request": "generators"}
        if(req == match):
            s = db.get_generators()
            return s
        else:
            raise Exception("Dictionnaire non Valid")
    else:
        return Exception("Dictionnaire Vide!!", req)


if __name__ == '__main__':
    # TEST FOR Docker
    RdosServer.server_conn('127.0.0.1', '9393')
