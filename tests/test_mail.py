import os
import sys
import socket
sys.path.append(os.path.realpath('../softwares/'))

import mailer.mail_templates as ms # noqa E402


# ensures that the connection is timed out after 1 second (in cases the test is not
# launched on the proper network
socket.setdefaulttimeout(1.0)


def test_empty_mail():
    assert ms.send_mail("", "") == 0
    assert ms.send_mail("", "1234") == 0

# assert ms.send_mail("julien.david@lipn.univ-paris13.fr","1234") == -1
