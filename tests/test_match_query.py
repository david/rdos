#!/usr/bin/env python3
# Authors : Julien DAVID & Ismail MOUMNI
import sys
import os
import pytest
sys.path.append(os.path.realpath('../softwares/'))
import server.server as srv  # noqa E402

data1 = {"id": "", "name": "", "state": "", "launch_date": "", "mdr_number": "", "nameC": {"first": "", "last": ""}, "title": "", "id": ""}
data2 = {"id": "", "name": "", "state": "", "launch_date": "", "mdr_number": "", "id": ""}


def test_match_query():
    var = srv.match_query_dict(data1, data1)
    assert var is not False


def test_diff_match_query():
    result = srv.match_query_dict(data1, data2)
    assert result is not None


def test_bad_match():
    with pytest.raises(Exception):
        assert srv.match_query_dict({}, data2)
