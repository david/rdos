#!/usr/bin/ python3
# Authors : Julien DAVID & Ismail MOUMNI
import sys
import os
import pytest
sys.path.append(os.path.realpath('../softwares/'))
import server.server as srv  # noqa E402


data1 = {
    "id": "",
    "name": "",
    "state": "",
    "launch_date": "",
    "mdr_number": "",
    "nces_id": "",
    "error": "",
    "sis_type": "",
    "pause_start": "",
    "pause_end": "",
    "district_contact": {
      "district": "",
      "email": "",
      "name": {
        "first": "",
        "last": "",
      },
      "title": "",
      "id": ""
    }
    }
data2 = {
    "id": "",
    "name": "",
    "state": "",
    "launch_date": "",
    "mdr_number": "",
    "nces_id": "",
    "error": "",
    "sis_type": "",
    "pause_start": "",
    "pause_end": "",
    "district_contact": {
      "district": "",
      "email": "",
      "name": {
        "first": "",
        "last": "",
      },
      "title": "",
      "id": ""
    }
    }


def test_missing_keys():
    var = srv.missing_keys(data1, data2)
    assert not var


def test_bad_missing():
    with pytest.raises(Exception):
        assert srv.missing_keys({}, data1)
