#!/usr/bin/ python3
# Authors : Julien DAVID & Ismail MOUMNI
import sys
import os
import pytest
import json
sys.path.append(os.path.realpath('../softwares/'))

import server.server as serv # noqa E402


data_test1 = '{"parameters": "request"}'
data_test2 = '{"Ref": "Parameters"}'


data_load = json.loads((data_test1))
data_load2 = json.loads(data_test2)


def test_query_valid():
    var = serv.query_valid(data_load)
    assert ((var) is True)


def test_bad_missing():
    with pytest.raises(Exception):
        assert serv.query_valid(data_load2)
