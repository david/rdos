import pytest
import os
import sys
sys.path.append(os.path.realpath('../softwares/'))

import database.database as db # noqa E402


def test_empty_password():
    print("Empty password test")
    with pytest.raises(db.NoPasswordDefineError):
        db.get_password()


def test_unsecurred_password():
    print("Unsecured password test")
    os.environ['rdos_secret'] = "test"
    assert db.get_password() == "test"
