<?php
  require_once('php/config.inc.php');
  require_once('php/functions.php');
  require_once('php/template.php');
  require_once('php/dynContent.php');
  require_once('php/menu.php');
  require_once('php/utils.php');
  require_once('php/urlOptions.php');
  require_once('php/options.php');
  require_once('php/db.php');

  $mainPage = 'index.php';

  // Retrieving and checking variables
  $opts = new URLOptions();
  $opts->assignOptions($_REQUEST);


  // Configuration
  $config =
    array(
	"*" => array(
		new DataSource("sql:SELECT * FROM news ORDER BY Date DESC;", 1, array(),5, "newsElement"),
		new DataSource("sql:SELECT * FROM generators ORDER BY tool,mode;", 1, array(),1000000, "generatorElement",array("tool"=>"generatorToolCategory")),
		),
	"search" => array(
		new DataSource("sql:SELECT * FROM generators ORDER BY paradigm;", 1, array(),1000000, "genElement",array("paradigm"=>"paradigmCat")),
		new DataSource("sql:SELECT * FROM generators ORDER BY outputFormat;", 1, array(),1000000, "genElement",array("outputFormat"=>"outputFormatCat")),
		),
	"tool" => array(
		new DataSource("sql:SELECT * FROM generators WHERE id='".$opts->getOption('generator')."';", 1, array(),1000000, "tool"),
		new DataSource("sql:SELECT * FROM generators as gen, parameters as params WHERE gen.id='".$opts->getOption('generator')."' AND params.idGenerator = gen.id ORDER BY rank,name;", 1, array(),1000000, "toolOpts"),
		),
    );


  if (!isset($languages[$opts->getLanguage()]))
  {   $opts->setOption('lang',$langDefault); }
  $lang = $opts->getLanguage();
  if (!isset($cssFiles[$opts->getCSS()]))
  {   $opts->setOption('css',$cssDefault); }
  $css = $cssFiles[$opts->getCSS()];

  $menu = loadMenu($XMLMenuFile,$opts);
  if (count($menu->getPathForPageID($opts->getPage()))==0)
  {   $opts->setOption('page',$pageDefault[$opts->getLanguage()]); }


  // Setting up template parameters
  $template = new Template('.');
  $template->assign_vars( array(
    'mainTitle' => "RDOS - Random Discrete Objects Suite",
    'CSSFile' => $css,
    'currentURL' => $opts->getCurrentURL(),
    'currentPage' => $opts->getPage(),
    'changeLanguage' => $opts->formatCurrentURLNoLanguage()."&amp;lang=",
    'changePage' => $opts->formatCurrentURLNoPage()."&amp;page=",
    'changePageGenerator' => $opts->formatUpdatedURL(array("generator" => '', "page" => 'tool'))."&amp;generator=",
    'linkEN' => $opts->formatUpdatedURLSingle('lang','en'),
    'linkFR' => $opts->formatUpdatedURLSingle('lang','fr'),
    'frenchVersion' => '<a href="'.$opts->formatUpdatedURLSingle('lang','fr').'">'.$flags["fr"].'</a>',
    'englishVersion' => '<a href="'.$opts->formatUpdatedURLSingle('lang','en').'">'.$flags["en"].'</a>',
    'frenchFlag' => ''.$flags["fr"].'',
    'englishFlag' => ''.$flags["en"].'',
    'flagFR' => ''.$flags["fr"].'',
    'flagEN' => ''.$flags["en"].'',
  ));


  $template->assign_block_vars('hierarchyElement', array(
        'pageid' => $opts->getPage(),
        ));

  if (isset($config["*"]))
  {
    foreach ($config["*"] as $includes)
    {
        $content = basicLoadNestedBlockContent($dbh,$includes,$lang);
		basicApplyNestedBlockContent($template,$content,$opts,$includes->ignoreLevel,"","","",$includes->requiredFields,$includes->maxNumRecords);
    }
  }

  // Page specific content
  if (isset($config[$opts->getPage()]))
  {
    if ($opts->getPage()=="publications")
      loadBiblio($includes->src,$template,$mainAuthors,$opts);
    else
	{
      foreach ($config[$opts->getPage()] as $includes)
      {
        $content = basicLoadNestedBlockContent($dbh,$includes,$lang);
		basicApplyNestedBlockContent($template,$content,$opts,$includes->ignoreLevel,"","","",$includes->requiredFields,$includes->maxNumRecords);
	  }
    }
  }
 $template->set_filenames(array(
    'body' => $opts->locatePage('base'),
    $opts->getPage() => $opts->locatePage($opts->getPage()),
  ));


  populateMenu("menuElement",$menu,$template);

  $template->assign_var_from_handle('content', $opts->getPage());
  $template->pparse('body');

?> 