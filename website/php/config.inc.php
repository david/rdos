<?php
  include_once("php/localconfig.inc.php");
  include_once("php/db.php");
  $dbh = connectDB();

  $XMLNewsFile = "xml/news.xml";
  $XMLMenuFile = "xml/menu.xml";
  $templateDir = './templates';
  $staticDir = './content';
  $jobsDir = './jobs';

    // Default values
  $langDefault = 'en';
  $cssDefault = 'bioinfo';
  $pageDefault = array('en'=>'home','fr'=>'home');
  $mainAuthors = array(
    "Yann Ponty" => 1,
    );


  // Localized dynamic contents
  $cssFiles = array(
    'bioinfo' => 'css/bioinfo.css',
    //'debug' => 'css/debug.css',
    //'none' => '',
  );

  // Available languages
  $languages = array('en'=> 'en','fr'=> 'fr');

    DEFINE ('JOB_CREATED',       0);
    DEFINE ('JOB_VALIDATED',    10);
    DEFINE ('JOB_QUEUED',      100);
    DEFINE ('JOB_RUNNING',    1000);
    DEFINE ('JOB_CANCELLED',   2000);
    DEFINE ('JOB_OUTPUT',    10000);
    DEFINE ('JOB_DONE',     100000);
    DEFINE ('JOB_DELETED', 1000000);

?>
