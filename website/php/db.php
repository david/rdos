<?php

function connectDB()
{
	$dbh =  new mysqli(DB_HOST, DB_USER, DB_PASSWORD,DB_NAME);
	$dbh->set_charset("utf8");
	return $dbh;
}

function createAsXML($dbh,$sqlquery,$contentName,$fullhierarchy,$lang)
{
	$dbresult = $dbh->query($sqlquery);
	$result = "";
	$result .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	$result .= "<content>\n";
	if ($dbresult)
	{
		$hierarchy = array_keys($fullhierarchy);
		$nbIndent = count($hierarchy);
		$prevVals = array();
		foreach ($hierarchy as $i=>$x)
		{
			$prevVals[$x] = -1;
		}
		
		$begin = true;
		while($data = $dbresult->fetch_assoc()) {
			foreach ($hierarchy as $i=>$x)
			{
				if ($prevVals[$x] != $data[$x])
				{
					if (!$begin)
					{  
						for ($j=count($hierarchy)-1; $j>=$i;$j--)
						{
							$x = $hierarchy[$j];
							$alias = $fullhierarchy[$x];
							$result .= str_repeat("\t", $j+1)."</$alias>\n";
						}
					}

					for ($j=$i; $j<count($hierarchy);$j++)
					{
						$x = $hierarchy[$j];
						$prevVals[$x] = $data[$x];
						$alias = $fullhierarchy[$x];
						$result .= str_repeat("\t", $j+1)."<$alias>\n";
						$result .= str_repeat("\t", $j+2)."<val><![CDATA[".($data[$x])."]]></val>\n";				
					}
					break;
				}
			}
			$begin = false;
			$result .= str_repeat("\t", $nbIndent+1)."<$contentName>\n";
			foreach($data as $key => $value) {
				$keymod = $key;
				if (endsWith( strtoupper($keymod),strtoupper("-".$lang)))
				{
					$keymod = substr($keymod,0,strlen($keymod)-strlen("-".$lang));
				}
				$result .= str_repeat("\t", $nbIndent+2)."<$keymod><![CDATA[".((($value)))."]]></$keymod>\n";
			}
			$result .= str_repeat("\t", $nbIndent+1)."</$contentName>\n";
		}
		if ($begin)
		{
			for ($j=0; $j<count($hierarchy);$j++)
			{
				$x = $hierarchy[$j];
				$alias = $fullhierarchy[$x];
				$result .= str_repeat("\t", $j+1)."<$alias>\n";
			}
		}
		for ($j=count($hierarchy)-1; $j>=0;$j--)
		{
			$x = $hierarchy[$j];
			$alias = $fullhierarchy[$x];
			$result .= str_repeat("\t", $j+1)."</$alias>\n";
		}
	}
	$result .= "</content>\n";
	return ($result);
}




?>