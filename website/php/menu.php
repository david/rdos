<?php

class Menu {
    var $dynContents;
    function Menu($content)
    {
        $this->dynContents = $content;
    }
    function getDynContent()
    { return $this->dynContents; }

    function getPathForPageID($id)
    {
       $result = NULL;
       foreach($this->dynContents as $index => $dyncont)
       {
         $tmp = $dyncont->search($id);
         if ($tmp!=NULL)
         {
           $result = $tmp;
         }
       }
       $lst = array();
       while (! is_null($result))
       {
         $lst[] =  $result->getID();
         $result = $result->getFather();
       }
       return $lst;
    }
}


function loadMenuAux($xml,$opts,&$result,$father)
{
    $dynContent = new DynContent($opts);
    $dynContent->setFather($father);
    $children = array();
    $id= -1;
    $hidden = False;
    foreach($xml->children() as $field)
    {
      $name = strtolower($field->getName());
      if (!strcmp($name,"id"))
      {
        $id = cleanup($field->asXML());
        $dynContent->setID($id);
      }
      else if (!strcmp($name,"menuitem"))
      {
         loadMenuAux($field,$opts,$children,$dynContent);
      }
      else
      {
        $dynContent->setData($field->getName(),html_entity_decode(cleanup($field->asXML())));
      }
    }
    if (!$hidden)
    {
      $dynContent->setChildren($children);
      $result[] = $dynContent;
    }
}

function loadMenu($XMLFile,$opts)
{
  $result = array();
  $xml = simplexml_load_file($XMLFile,'SimpleXMLElement',LIBXML_NOCDATA);
  foreach($xml->children() as $xmlChild)
  {
     loadMenuAux($xmlChild,$opts,$result,NULL);
  }
  return new Menu($result);
}

function populateMenu($category,$menu,$template)
{
  $blockName = $category;
  $subBlockName = $blockName.'.sub'.$blockName;
  foreach($menu->getDynContent() as $index => $content )
  {
    if (!$content->isHidden())
    {
    $template->assign_block_vars($blockName, $content->getAllLocData());
    foreach($content->getChildren() as $subindex => $subcontent )
    {
      $template->assign_block_vars($subBlockName, $subcontent->getAllLocData());
    }
    }
  }
}



?>