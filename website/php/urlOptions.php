<?php

  // URL options management
  class URLOptions
  {
    var $opts = array();

    function URLOptions()
    {
      global $langDefault;
      global $pageDefault;
      global $cssDefault;
      $this->opts['lang'] = $langDefault;
      $this->opts['page'] = $pageDefault[$this->opts['lang']];
      $this->opts['css'] = $cssDefault;
    }
    function getPage()
    { return $this->opts['page']; }
    function getLanguage()
    { return $this->opts['lang']; }
    function localize($name)
    {
      $res = "";
      $lang = $this->opts['lang'];
      if (endswith(strtolower($name),"-".strtolower($lang)))
        $res = substr( $name, 0, strlen( $name ) - (strlen( $lang )+1) );
      else
        $res = $name;
      //echo $name."->".$res."<br>";
      return $res;
    }
    function getCSS()
    { return $this->opts['css'];  }
    function getURLForPage($page)
    { return $this->formatUpdatedURLSingle('page',$page); }
    function formatTruncatedURL($key)
    {  return $this->formatUpdatedURLSingle($key,''); }
    function formatCurrentURLNoPage()
    { return $this->formatTruncatedURL('page');}
    function formatCurrentURLNoLanguage()
    { return $this->formatTruncatedURL('lang');}
    function formatUpdatedURLSingle($nkey,$nval)
    { return $this->formatUpdatedURL(array($nkey=>$nval)); }
    function getCurrentURL()
    {
      return $this->formatUpdatedURL(array());
    }
    function formatUpdatedURL($tab)
    {
      global $langDefault;
      global $pageDefault;
      global $cssDefault;
      global $mainPage;
      $result=$mainPage.'?';
      $first = 1;
      foreach( $this->opts as $key => $value )
      {
        $val = $value;
        if (isset($tab[$key]))
        {$val = $tab[$key];}
        if (strcmp($val,''))
        {
		  if ($key=="lang" && $val==$langDefault)
		  {}
	      else if ($key=="page" && $val==$pageDefault[$this->getLanguage()])
		  {}
	      else if ($key=="css" && $val==$cssDefault)
		  {}
          else
		  {
            if (!$first)
            { $result .= "&amp;"; }
            $result .= $key."=".urlencode($val);
            $first = 0;
		  }
        }
      }
      return $result;
    }

    function assignOptions($tab)
    {
      foreach( $tab as $key => $value )
      {
        if (isset($tab[$key]))
        { $this->setOption($key,$tab[$key]); }
      }
    }
    function restrictTo($tab)
    {
      foreach( $this->opts as $key => $value)
      {
        if (!isset($tab[$key]))
        { $this->removeOption($key); }
      }
    }
    function removeOption($key)
    { unset($this->opts[$key]); }
    function setOption($key,$val)
    { $this->opts[$key] = $val; }
    function getOption($key,$default = "")
    { 
		if (isset($this->opts[$key]))
			return $this->opts[$key]; 
		else
		  return $default;
	}

    function locatePage($page)
    {
      global $templateDir;
      global $staticDir;
      global $DS;
      $dynext = "tpl";
      $staext = "html";
	  $lan = $this->getLanguage();
      $filename = $templateDir.$DS.$page."-".$lan.".".$dynext;
      if (file_exists($filename))
      { return $filename; }
      $filename = $staticDir.$DS.$page."-".$lan.".".$staext;
      if (file_exists($filename))
      { return $filename; }
      $filename = $templateDir.$DS.$page.".".$dynext;
      if (file_exists($filename))
      { return $filename; }
      $filename = $staticDir.$DS.$page.".".$staext;
      if (file_exists($filename))
      { return $filename; }
      throw new Exception('Unable to locate file ['.$page.']!');
    }
  }

?> 