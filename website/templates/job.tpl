<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
    <HEAD>
        <TITLE>{mainTitle}</TITLE>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <LINK rel="Stylesheet" type="text/css" href="css/tex4ht.css">
        <LINK rel="Stylesheet" type="text/css" href="{CSSFile}">
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400italic,400,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>
		
		<link rel="stylesheet" href="js/jQueryUI/css/smoothness/jquery-ui-1.10.3.custom.min.css" />
        <script type='text/javascript' src='js/jQueryUI/js/jquery-1.9.1.js'></script>
        <script type='text/javascript' src='js/jQueryUI/js/jquery-ui-1.10.3.custom.min.js'></script>
		
		<script src="js/d3.v3.js"></script>
        <noscript>
			{HTMLRefresher}
		</noscript>
        <STYLE type="text/css"><!--
           div#contentContainer{ 
			 width:100%;
		   }
           #menuElement{currentSection}
           { 
             border-left:5px solid #65944A;
             color:#65944A;
             padding:0.3em 0 0.3em 10px;
             border-top:1px dashed #D4D4D4;
           }
          <!-- BEGIN hierarchyElement -->
           #menuElement{hierarchyElement.pageid} div
           { 
             display: block;
           }
          <!-- END submenuElement -->
        --></STYLE>
		
        <script type='text/javascript'>
		var needsRefresh = {needsRefresh};

		function getURL()
		{
			var url = "{currentURL}";
			return url.replace(/&amp;/g,"&");
		}
		
		function reloadPage()
		{
			$.ajax({type: "POST",url: getURL()+"&ajax=1&ajaxType=status",data: { }})
						.done(function( msg ) {
							$("#content").removeClass("loading"); 
							$("#statusContent" ).html(msg);
							rerouteButton();
							if (to!=-1){
								clearTimeout(to);
							}
							if (needsRefresh)
							{
								to = setTimeout(function(){reloadPage();}, 2000 ); 
							}
							else
							{
								$.ajax({type: "POST",url: getURL()+"&ajax=1&ajaxType=result",data: { }})
									.done(function( msg ) {
									$("#resultContent" ).html(msg);
									$( "#jobcontent" ).accordion( "option", "active", 2 );
								});
								
							}

						});
		}
		
		function rerouteButton()
		{
				$( ".refreshButton").on( "click", function( event ) {
					reloadPage();
				});
		}
		
		var to = -1;
		
		$(document).ready(
			function(){
				$(document).on({
					ajaxStart: function() { 
						$("#content").addClass("loading"); 
					},
					ajaxStop: function() { 
						$("#content").removeClass("loading"); 
					}
					});
				rerouteButton();
			});
			if (needsRefresh)
			{
				to = setTimeout(function(){ 
					reloadPage();
				}, 5000 ); 
			}
			else
			{
				$("#content").removeClass("loading");
				if (to!=-1){
					clearTimeout(to);
			}

			}
  </script>
    </HEAD>
<BODY class="mainBackground">
  <div id="mainContainer">
  <div class="shadow">
  <div id="mainContent">
    <div id="mainHeader">
      <div id="photo">
      <a href="."><img src="pics/rdosByAndrea.png" alt="(E)RDOS by Andrea Sportiello" title="RDOS by Andrea Sportiello" id="rdosLogo"></a>
      </div>
      <div id="lang"> 
        <a href="{linkFR}"><img src="pics/flagfr.png" alt="French version"></a> <a href="{linkEN}"><img src="pics/flagen.png" alt="English version"></a>
      </div>
      <div id="mainTitle">
        {mainTitle}
      </div>
      <div class="filler"></div>
    </div>
    <div id="contentContainer">
      <div class="shadow">
      <div id="content">
        {content}
      </div>
      </div>
    </div>
    <div id="mainFooter">
      Website Copyleft Yann Ponty &amp; Julien David 2014
    </div>
  </div>
  </div>
  </div>
</BODY>
</HTML>

